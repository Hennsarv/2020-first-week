﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Teisendused
{
    class Program
    {
        static void Main(string[] args)
        {
            // vaatame täna kõigepealt, kuidas andmetüppisid omavahel sõbraks teha
            // ehk teisendada ühest teiseks

            // jämedas lõikes on meil arvud, tekstid, kuupäevad-kellaajad
            // (muid pole jõudnud vaadata)

            int lyhike = 500000000; // muutujat lyhike kasutan ma lihtsalt niisama
            Console.WriteLine($"muutuja lyhike väärtus on {lyhike}");

            long pikk = lyhike;
            short teine = (short)(pikk + (7)); // casting tehe

            // 

            string s = "see asi on " + (4 + 7 * teine).ToString(); // siin ma teen ise
            s = string.Format("see asi on {0}", 4 + 7 * teine);
            // string.Format kasutab placeholderite kohal to stringi
            Console.WriteLine(s);
            Console.WriteLine("see asi on {0}", 4 + 7 * teine);
            Console.WriteLine($"see asi on {4 + 7 * teine}");
            // WriteLine kasutab string.Formatit
            // string.Format kasutab ToStringi
            // $-string avaldis kasutab sisemiselt string.Formatit

            // ToString on alles JÄÄMÄE veepealne osa
            // mõned näited veel

            double d = 1.0 / 7;
            Console.WriteLine("{0 :F2}",d);

            Console.WriteLine("täna on {0:d.MMMM.yyyy}", DateTime.Today);
            string täna = DateTime.Today.ToString("d.MMMM.yyyy dddd", new CultureInfo("fi-fi"));
            Console.WriteLine(täna);

            // suvaline andmetüüp (arv, kuupäev, kellaaeg) ToString() - teisendab stringiks

            // kuidas saaks vastupidi
            Console.Write("mis päeval sa sündinud oled: ");
            //DateTime sünnipäev = DateTime.Parse(Console.ReadLine());
            //Console.WriteLine(sünnipäev);
            //Console.WriteLine($"Sa oled siis {(DateTime.Today - sünnipäev).TotalDays} päva vana");

            //double testDouble = double.Parse("47,89");

            if(DateTime.TryParse(Console.ReadLine(), out DateTime sünnipäev))
            {
                Console.WriteLine(sünnipäev);
                // kuidas arvutada vanust ???
                // sinu vanus (kuidas arvutada)
                Console.WriteLine($"sinu vanus on {(DateTime.Today - sünnipäev).Days * 4 / 1461} ");
            }
            else
            {
                Console.WriteLine("see pole miski kuupäev");
            }

            // Convert klass sisaldab SUURE hulga teisendusfunktsioone
            Console.Write("anna üks arv: ");
            int arv = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(arv);



            // kuidas arvutada tulumaksu

            Console.Write("Mis su palk on: ");
            if (decimal.TryParse(Console.ReadLine(), out decimal palk))
            {
                Console.WriteLine($"sinu tulumaks on: ???");
                // alla 500€ puhul on tulumaks 0
                // üle 500 puhul on tulumaks 500 ületavalt osalt 20%
                
                //decimal tulumaks = 0M;
                //if (palk > 500) tulumaks = (palk - 500) / 5;

                decimal tulumaks = palk < 500M ? 0M : (palk - 500M) * 0.2M;
                
                Console.WriteLine($"sinu tulumaks on: {tulumaks}");

            }
            else Console.WriteLine("see pole miski palk");





        }
    }
}
