﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsüklid
{
    class Program
    {
        static void Main(string[] args)
        {
            // täitsa uus teema - mitu korda ma pean sama meest maha lööma
            // Stjopa - me loodame, et nüüd töötab ka salvestamine
            // ikka ei õnnestunud salvestamine - ma ka ei tea miks

            // KUi mõnikord on vaja teha SAMA asja mitu korda
            // siis selleks on teistsugune plokk - tsükkel
            ;
            int[] arvud = new int[8];
            for (int i = 0; i < arvud.Length; i++)
            {
                Console.WriteLine($"Teen seda asja {i}. korda");
                arvud[arvud.Length - i - 1] = i * i;
            }


            foreach (int x in arvud) // for (int x : arvud)
            {
                Console.Write($"{x}, ");
            }
            Console.WriteLine();

            #region kommenteerisin välja mõõned asjad

            //while(bool-avaldis) // korratakse seni kui avaldis on true
            //{

            //}

            //do
            //{

            //} while (bool - avaldis) ; // korratakse seni kui avaldis on true

            #endregion

            int[,] tabel = new int[10, 10];
            for (int i = 0; i < tabel.GetLength(0); i++)
                for (int j = 0; j < tabel.GetLength(1); j++)
                {
                    tabel[i, j] = i * j;
                }
            //            foreach (var x in tabel) Console.Write($" {x}");

            for (int n = 0; n < 100; n++)
            {
                Console.Write($"{tabel[n / 10, n % 10]:00} {(n % 10 == 9 ? "\n" : "")}");
            }

            int[][] teineTabel = new int[10][];
            for (int i = 0; i < teineTabel.Length; i++)
            {
                teineTabel[i] = new int[10];
                for (int j = 0; j < teineTabel[i].Length; j++)
                {
                    teineTabel[i][j] = i * j;
                }
            }

            foreach (var r in teineTabel)
            {
                foreach (var x in r) Console.Write($"{x:00} ");
                Console.WriteLine();
            }
            Random r = new Random();
            

        }
    }
}
