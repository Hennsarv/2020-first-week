﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TsükliÜlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime[] sünniKuupäevad =
            {
                new DateTime(1955,03,07),
                new DateTime(1970,01,01),
                new DateTime(1987,02,02),
                new DateTime(1960,04,04),
                new DateTime(1942,05,05),
            };
            String[] nimed =
            {
                "Henn",
                "Ants",
                "Peeter",
                "Kalle",
                "Joosep",
            };

            int vanusteSumma = 0;
            Console.WriteLine($"Meil on {nimed.Length} poissi");
            for (int i = 0; i < nimed.Length; i++)
            {
                Console.WriteLine($"{nimed[i]} kes on sündinud {sünniKuupäevad[i]:d.MMMM.yyyy}");
                vanusteSumma += (DateTime.Today - sünniKuupäevad[i]).Days * 4 / 1461;
            }
            Console.WriteLine($"Keskmine vanus on {(vanusteSumma * 1.0 / nimed.Length) :F2}");

            // kuidas leida kõige hilisem kuupäev massiivist
            DateTime hiliseim = sünniKuupäevad[0];
            int kelleoma = 0;
            for (int i = 1; i < sünniKuupäevad.Length; i++)
            {
                if(sünniKuupäevad[i] > hiliseim)
                {
                    hiliseim = sünniKuupäevad[i];
                    kelleoma = i;
                }
            }
            Console.WriteLine($"Kõige noorem on {nimed[kelleoma]}");
            Console.WriteLine($"ta on {(DateTime.Today -sünniKuupäevad[kelleoma]).Days * 4 / 1461} aastane");

            int kogum = 100;
            int vahemik = 100;

            int[] juhuslikud = new int[kogum];
            Random r = new Random();


            int suurim = -1;
            int väikseim = vahemik;
            int suurimaKandidaat = 0;
            int väikseimakandidaat = 0;
            int summa = 0;
            int[] mitumida = new int[vahemik];

            for (int i = 0; i < juhuslikud.Length; i++)
            {
                int juhus;

                juhuslikud[i] = juhus = r.Next(vahemik);
                mitumida[juhus]++;
                Console.Write($"{juhus:00}{(i % 10 == 9 ? "\n" : " ")}");
                if (juhus > juhuslikud[suurimaKandidaat])
                {
                    suurimaKandidaat = i; suurim = juhus;
                }
                if (juhus < juhuslikud[väikseimakandidaat])
                {
                    väikseimakandidaat = i; väikseim = juhus;
                }
                summa += juhus;
            }
            Console.WriteLine($"suurim on {suurim} väikseim on {väikseim} keskmine on {summa * 1.0 / juhuslikud.Length}");
            // Console.WriteLine($"suurim on {juhuslikud.Max()}");
            // Console.WriteLine($"Keskmine on {juhuslikud.Average()}");
            // mis ma peaks tegema, et leida suurim? väikseim? keskmine?
            // paneme kandidaadiks kellegi ja hakkame teisi temaga võrdlema
            string pole = "";
            string palju = "";
            int enim = mitumida.Max();
            for (int i = 0; i < mitumida.Length; i++)
            {
                if (mitumida[i] == 0) pole += $"{i:00} ";
                if (mitumida[i] == enim) palju += $"{i:00} ";
            }
            
            if(pole == "")
                Console.WriteLine("Tabelis on kõiki arve");
            else
                Console.WriteLine($"Tabelis ei ole {pole} mitte ühtegi");
            
            Console.WriteLine($"enim on {palju} - neid on {enim} tükki");

            // kuidas leida, millist arvu on kõikse rohkem
            // ja milline (millised) need on



        }
    }
}
