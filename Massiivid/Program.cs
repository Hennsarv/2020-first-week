﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] arvud = new int[10];  // massiiv ehk array

            int[] teised = new int[] { 1, 2, 7, 3, 6, 8, 11 };
            int[] kolmas = { 1, 2, 7, 3, 6, 8, 11 };
            arvud = new int[] { 1, 2, 3 };
            Console.WriteLine(teised[3]);
            Console.WriteLine(string.Join(", ", teised));

            // harilikult ma neid kahte ei õpeta esimese nädala kolmapäeval
            int[] yhestKümneni = Enumerable.Range(1, 10).ToArray(); // keeruline 1
            Console.WriteLine(string.Join(", ", yhestKümneni));     // keeruline 2

            // kuna Stepan on haige, siis ma igaks juhuks selle asja salvestan
            // kahju, et ma hommikul seda meeles ei pidanenud
            // Stjopa - kahjuks meil ei salvestand, pean sulle asja üle rääkima
            
            // massiivid (ja muud samasugused loomad) mis koosnevad eri tüüpi asjadest on pahad kasutada
            object[] eriTüüpi = new object[4]; // siin võivad olla suva asjad, aga jubeebamugav
            eriTüüpi[1] = "Henn";
            eriTüüpi[2] = 64;
            eriTüüpi[0] = "";
            eriTüüpi[3] = DateTime.Now;
            Console.WriteLine(string.Join(", ", eriTüüpi));
            // ära nii tee

            int[][] misSeeOn = new int[3][];
            misSeeOn[0] = new int[] { 1, 2, 3 };
            misSeeOn[1] = new int[] { 4, 5 };
            misSeeOn[2] = new int[] { 7, 8, 9, 10 };
            int[][] seeOn = { new int[]{ 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } };

            // int[][] EI OLE (aga paistab nagu oleks) kahemõõtmeline massiiv
            // see on massiiv mis koosneb massiividest

            int[,] tabel = new int[8, 8]; // see on 2-mõõtmeline massiiv
            int[,] tabel2 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };


        }
    }
}
