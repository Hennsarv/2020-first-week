﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReedeneHarjutus3
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Palgad.txt";
            Dictionary<string, (string nimi, string osakond, decimal palk)> palgaTabel 
                = new Dictionary<string, (string nimi, string osakond, decimal palk)>();
            // algul mõtlesin, et teen listi
            // aga et mul on vaja nime järgi otsida ja muuta, siis tegin dictionariks
            // uuel nädalal, kui oskame classe kasutada, võiks lihtsamaks asjaks teha

            #region Muutmise näidis
            // kuidas ma muudan jagaupi palka
            //var jaagup = palgaTabel["Jaagup"];
            //jaagup.palk += 1000;
            //palgaTabel["Jaagup"] = jaagup;

            #endregion
            // sisse loen ma nii - seda teeme enne töö algust
            var loetudRead = File.ReadAllLines(filename);
            for (int i = 1; i < loetudRead.Length; i++)
            {
                var loetudRida = loetudRead[i].Split(',');
                var palgaRida = (loetudRida[0],loetudRida[1], decimal.Parse(loetudRida[2]));
                palgaTabel.Add(loetudRida[0], palgaRida);
            }
            bool kasSalvestada = false;  // selle muutuja võtan kasutusele, et muudatusi jälgida
            // kui muudatusi ei ole, siis ei ole vaja salvestada
            // kui teen muudatuse, siis muudan seda muutujat


            // nüüd oleks vaja see küsimise vastamise asi teha
            Console.WriteLine("Tere Henn - olen meie firma palgaprogramm");
            while (true)
            {
                Console.WriteLine("Mida tahad teha?");
                Console.Write("v - vaadata, m - muuta, l - lisada, k - kokku võtta, s- salvestada, x - lõpetada: ");
                string vastus = Console.ReadLine().ToLower().Substring(0,1);
                // x lubab väljuda vaid siis, kui ei ole salvestamata muudatusi
                // z lubab väljuda ka siis, kui on salvestamata (jätab salvestamata)

                if (vastus == "x" && !kasSalvestada || vastus == "z") break;
                switch(vastus)
                {
                    case "v": // vaatamine
                        foreach (var x in palgaTabel) Console.WriteLine(x); 
                        // TODO: ehh pärast teeme ilusaks
                        break;
                    case "x": // enne lõpetamist vaja salvestada
                        Console.WriteLine("sa oled teinud muudatusi - enne oleks need vaja salvestada");
                        Console.WriteLine("kui ei taha salvestada vali x asemel z");
                        break;
                    case "s": // salvestamine
                              // salvestan ma nii
                        {
                            List<string> salvestamiseks = new List<string> { "Nimi,Osakond,Palk" };
                            foreach (var x in palgaTabel.Values)
                                salvestamiseks.Add(x.ToString().Replace(")", "").Replace("(", "").Replace(", ", ","));
                            File.WriteAllLines(filename, salvestamiseks);
                            kasSalvestada = false;
                        }
                        break;
                    case "m": // muutmine
                        {
                            Console.Write("Kelle palka muudame: ");
                            string nimi = Console.ReadLine();
                            if (nimi != "" && palgaTabel.ContainsKey(nimi))
                            {
                                Console.Write("Mis me talle palgaks paneme: ");
                                if (decimal.TryParse(Console.ReadLine(), out decimal palk))
                                {
                                    if (palk > palgaTabel[nimi].palk)
                                    {
                                        var kes = palgaTabel[nimi];
                                        kes.palk = palk;
                                        palgaTabel[nimi] = kes;
                                        kasSalvestada = true;
                                    }
                                    else Console.WriteLine("palka saab vaid suurendada - A/Ü nõuab");
                                }
                            } else if (nimi != "") Console.WriteLine("sellist ei ole");
                        }
                        break;
                    case "l": // lisamine
                        {
                            Console.Write("Kelle me lisame: ");
                            string nimi = Console.ReadLine();
                            if (nimi != "")
                            {

                                if (!palgaTabel.ContainsKey(nimi))
                                {
                                    Console.Write("Kuhu osakonda: ");
                                    string osakond = Console.ReadLine();
                                    if (osakond != "")
                                    {
                                        Console.Write("Palgaks paneme: ");
                                        if (decimal.TryParse(Console.ReadLine(), out decimal palk))
                                        {
                                            palgaTabel.Add(nimi, (nimi, osakond, palk));
                                            Console.WriteLine("lisatud");
                                            kasSalvestada = true;
                                        }
                                    }
                                }
                                else Console.WriteLine("Selline nimi juba on");
                            }
                        }
                        break;
                    case "k": // kokku võtmine
                        {
                            Dictionary<string, List<decimal>> kokkuvõte = new Dictionary<string, List<decimal>>();
                            int mitu = 0;
                            decimal kokku = 0;
                            foreach (var x in palgaTabel.Values)
                            {
                                mitu++; kokku += x.palk;
                                if (!kokkuvõte.ContainsKey(x.osakond)) kokkuvõte[x.osakond] = new List<decimal>();
                                kokkuvõte[x.osakond].Add(x.palk);
                            }
                            foreach (var x in kokkuvõte)
                            {
                                Console.WriteLine($"{x.Key}\t\tpalgad: {x.Value.Sum()}\tkeskmine: {x.Value.Average()}");
                            }
                        }
                        break;
                    default:
                        Console.WriteLine("ei mõista su soovi - loe ilusti ja vasta viisakalt");
                        break;
                }

            }


        }
    }
}
