﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeneHarjutus1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            // ära usu, et nii saab, sina ikka tee tsükkel
            //var arvud = Enumerable.Range(0, 100).Select(x => r.Next(100)).ToArray();
            var arvud = new int[100];
            for (int i = 0; i < 100; i++)
            {
                arvud[i] = r.Next(100);
            }
            /*
            Console.Write("\t\t");
            Enumerable.Range(1, 10).ToList().ForEach(x => Console.Write($"|{x}\t"));
            Console.WriteLine();
            Console.WriteLine(new string('─', 16)+ "|" + new string('─', 84));
            */
            for (int i = 0; i < arvud.Length; i++)
            {
                Console.Write((i%10 == 0 ? $"rida {i/10 + 1}\t\t|" : "") + $"{arvud[i]:00}" + (i % 10 == 9 ? "\n" : "\t"));
            }
            Console.WriteLine(new string('─', 16) + "|" + new string('─', 84));

            while (true)
            {
                Console.Write("Keda otsime: ");
                string otsitav = Console.ReadLine();
                if (otsitav == "") break;
                if (int.TryParse(otsitav, out int arv) && arv >= 0 && arv < 100)
                {
                    int mitu = 0;
                    for (int i = 0; i < arvud.Length; i++)
                    {
                        if (arvud[i] == arv)
                        {
                            mitu++;
                            Console.WriteLine($"leidsin selle sulle {i/10+1} reast ja {i%10+1} veerust");
                        }
                    }
                    // kaks varianti kuidas raporteerida tulemust - Iff iga
                    if (mitu == 0) Console.WriteLine("sellist ei leidnud");
                    else if (mitu == 1) Console.WriteLine("leidsin vaid ühe");
                    else Console.WriteLine($"leidsin lausa {mitu} tükki");

                    // või siis lasta Elvisel laulda
                    Console.WriteLine(
                        mitu == 0 ? "sellist ei leidnud ühtegi" :
                        mitu == 1 ? "sellist leidsin vaid ühe" :
                        $"leidsin sellist {mitu} tükki"
                        );
                }
                else Console.WriteLine("sellist ma parem ei hakkagi otsima");
            }

        }
    }
}
