﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelListeJaMassiive
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 7, 2, 8, 3, 9, 10 };
            string[] nimed = { "Henn", "Ants", "Peeter" };
            Array.Sort(arvud);
            Array.Sort(nimed);
            Console.WriteLine(string.Join(",", arvud));
            Console.WriteLine(string.Join(",", nimed));

            // lisaks veel mõned sugulased

            SortedSet<int> arvudSorted = new SortedSet<int>() { 1, 7, 2, 8, 3, 9, 10 };

            arvudSorted.Add(5);
            arvudSorted.Add(2);
            
            Console.WriteLine(string.Join(",", arvudSorted));

            arvud = arvudSorted.ToArray();
            arvud[0] += 7;
            arvudSorted = new SortedSet<int>(arvud);

            Dictionary<string, int> nimekiri = new Dictionary<string, int>
            {
                {"Henn", 64 },
                {"Ants", 28 },
                {"Peeter", 40 }
            };

            nimekiri.Add("Kalle", 20);

            var vanus = nimekiri["Peeter"];

            foreach (var x in nimekiri.Keys) Console.WriteLine($"{x} vanus on {nimekiri[x]}");

            var keskmine = nimekiri.Values.Average();

            //Console.Write("Kelle vanust sa tahad: ");
            //Console.WriteLine(nimekiri[Console.ReadLine()]);

            SortedList<string, List<int>> õpilasteHinded = new SortedList<string, List<int>>();

            õpilasteHinded.Add("Henn", new List<int>());
            õpilasteHinded.Add("Peeter", new List<int>());

            õpilasteHinded.Add("Ants", new List<int> { 5, 5, 4, 2 });
            õpilasteHinded["Henn"].Add(5);

            foreach(var x in õpilasteHinded)
                Console.WriteLine($"{x.Key} keskmine hinne on {x.Value.DefaultIfEmpty().Average()}");

            Console.WriteLine("\nmis on järjekord e queue\n");
            Queue<int> järjekord = new Queue<int>();
            järjekord.Enqueue(1);
            järjekord.Enqueue(7);
            järjekord.Enqueue(8);
            järjekord.Enqueue(3);
            järjekord.Enqueue(2);
            järjekord.Enqueue(10);
            järjekord.Enqueue(0);

            while(järjekord.Count() > 0 )
                Console.WriteLine(järjekord.Dequeue());

            Console.WriteLine("\nmis on kuhi e stack\n");
            Stack<int> kuhi = new Stack<int>();
            kuhi.Push(1);
            kuhi.Push(7);
            kuhi.Push(8);
            kuhi.Push(3);
            kuhi.Push(2);
            kuhi.Push(10);
            kuhi.Push(0);

            while(kuhi.Count() > 0)
                Console.WriteLine(kuhi.Pop());



        }
    }
}
