﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kuupäevad
    // ära kasuta võtmesõnu oma nimedena. Samuti süsteemseid klassinimesid
    // see on kommentaar mulle enesele!
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine(dt);

            dt = new DateTime(9999, 12, 31, 10, 17, 18);
            Console.WriteLine(dt);
            dt = DateTime.Parse("7. märts 1955");
            Console.WriteLine(dt);

            Console.WriteLine(DateTime.MinValue);
            Console.WriteLine(DateTime.MaxValue);
            Console.WriteLine(DateTime.Now.DayOfWeek);

            Console.WriteLine($"{DateTime.Now : dd.MMMM.yyyy (dddd)}");

            //while (true)
            //{
            //    Console.Write($"\r{DateTime.Now: hh:mm:ss} {DateTime.Now.Ticks % 10000000}");
            //}

            var x = DateTime.Today - dt; // Hennu sünnipäev

            Console.WriteLine(x.TotalDays);





        }
    }
}
