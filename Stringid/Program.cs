﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stringid
{
    class Program
    {
        static void Main(string[] args)
        {
            string nimi1 = "HENN Sarv";
            // üks vahva andmetüüp on 'System.String' e 'string'
            Console.WriteLine(nimi1);
            Console.WriteLine(nimi1[7]); // hetkel ei mõtle sellest
            // string on tekst - suvaline tekst maks pikkusega 2GB
            // võib sisaldada kõiki UNICODE (16bit) märke

            // stringil on üks lihtne tehe + (ja loomulikult +=)

            string nimi = "Henn";
            nimi += " ";
            nimi += "Sarv";
            Console.WriteLine(nimi);

            // erinevad viisid
            Console.WriteLine(nimi == nimi1); // NB! Javas saad vastuseks false
            Console.WriteLine(nimi.Equals(nimi1));   // see annab ka jaavas Jahh
            Console.WriteLine(nimi.Equals(nimi1, StringComparison.OrdinalIgnoreCase));   // see annab ka jaavas Jahh
            Console.WriteLine(String.Equals(nimi, nimi1, StringComparison.OrdinalIgnoreCase));

            //Console.Write("Kes sa sihuke oled: ");
            //nimi = Console.ReadLine();
            //Console.WriteLine("Tere " + nimi + "!" + "\n" + "kuidas sul läheb");

            Console.WriteLine("Ants on tore poiss\rHenn");
            Console.WriteLine("Henn\tAnts\tPeeter");
            Console.WriteLine("64\t32\t40");
            Console.WriteLine("Luts jutustab: \"Kui Arno isaga jne...");

            //string filename = "C:\\Users\\sarvi\\source\\repos\\2020-first-week\\Stringid\\TextFile1.txt";
            string filename2 = @"..\..\TextFile1.txt";

            string sisu = System.IO.File.ReadAllText(filename2);
            Console.WriteLine(sisu);

            Console.WriteLine("Meil on tore poiss {0} ta on {1} aastat vana", nimi, 64);
            Console.WriteLine("Meil on {1} aastane {0}", nimi, 64);

            Console.WriteLine($"Meil on {64} aastane {nimi}");
            string subfolder = "oluline";

            filename2 = $@"..\..\{subfolder}\filename.txt";
            // näide kuidas kahte asja $ ja @ koos kasutada 
            // c# 8 võib @$ ka teises järjekorras

            string arno = @"Luts jutustab: ""Kui Arno jne...""";
        }
    }
}
