﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatukeneTuplitest
{
    static class Program
    {
        static int Summa(this IEnumerable<int> m)
        {
            int summa = 0;
            foreach (var x in m) summa+=x;
            return summa;
        }

        static int Mitu(this IEnumerable<int> m)
        {
            int mitu = 0;
            foreach (var x in m) mitu++;
            return mitu;
        }

        static Double Keskmine(this IEnumerable<int> m)
        {
            double sum = 0;
            int mitu = 0;
            foreach(var x in m) { sum += x; mitu++; }
            return mitu == 0 ? 0.0 : sum / mitu; 
        }

        static (int summa, int mitu, double keskmine) Arvuta (this IEnumerable<int> m)
        {
            int sum = 0;
            int mitu = 0;
            foreach (var x in m) { sum += x; mitu++; }
            return (sum, mitu,   mitu == 0 ? 0.0 : sum*1.0 / mitu);
        }

        static void Main(string[] args)
        {
            // kus on hästi palju kasu tuplitest - funktsioonid
            //var arvud = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //Console.WriteLine(arvud.Summa());
            //Console.WriteLine(arvud.Keskmine());

            //Console.WriteLine(arvud.Arvuta());
            //Console.WriteLine(arvud.Arvuta().summa);
            //Console.WriteLine(arvud.Arvuta().keskmine);


            // lihtsalt et oleks ettekujutus, mis asjad need on
            Console.WriteLine((3, 4)); // sulgudes on kaks (võib ka rohkem) avaldist



            (int, int) paar = (7, 8);
            Console.WriteLine(paar);

            Console.WriteLine(paar.Item1); // saab ka tupli "tükkide" poole pöörduda
            paar.Item1++;

            // tuplid on natuke nutikamad, kui lihtsalt kahest tükist koosnev struktuur

            string nimi = "Henn";
            int vanus = 64;

            // tuple võib olla ka "ajutine" pandud kokku kahest "päris asjast" selle käsu ajaks
            (nimi, vanus) = ("Peeter", 40);
            Console.WriteLine((nimi, vanus));

            (int, int, int) kolmik = (7, 8, 9);
            // tükkide vaikimisi nimed on Item1, Item2, ...

            (int vasak, int parem) = (7, 8); // kaks muutujat ja kaks avaldist
            (int vasak, int parem) nimegaPaar = (7, 8);

            nimegaPaar.parem++; // nüüd on tükkidel nimed 

            (int vasak, int parem, int, int neljas) ohoo = (3, 4, 5, 6); // nelik
            ohoo.Item3++;  // nimeta tüki nimi on ikka Item3

            // vaatame veel üht varianti

            //(int v, int n) = ohoo; // ei toimi - paar ja nelik
            (int v, _, _, int n) = ohoo;

            // tuplitest saab teha igasuguseid asju

            (int nr, string nm)[] tupliMass1 = new (int, string)[10];
            tupliMass1[0].nr++;

            Dictionary<(int, int), string> tupligaDict = new Dictionary<(int, int), string>();

            tupligaDict[(2, 7)] = "Henn";

            Dictionary<int, (string nimi, int vanus)> nimekiri = new Dictionary<int, (string nimi, int vanus)>
            {
                {1, ("Henn", 64) },
                {2, ("Ants", 28) },
                {3, ("Peeter", 40) },
            };

            int peetriVanus = nimekiri[3].vanus;
            //nimekiri[2].vanus = 30; // nii ei saa teha
            // var x = nimekiri[2];  
            // x.vanus++;             // sellest poleks kasu
            // nimekiri[2] = x;       // nii saab, aga ebamugav
            Console.WriteLine(string.Join(",", nimekiri));


        }
    }
}
