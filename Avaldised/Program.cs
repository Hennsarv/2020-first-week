﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avaldised
{
    class Program
    {
        static void Main(string[] args)
        {
            int arv = 11;
            int teinearv = 0;
            Console.WriteLine( - (arv % 3));

            Console.WriteLine(  arv > 10 ? "tore" : "kurb");

            Console.WriteLine( (arv = 20) + (teinearv = 7));
            Console.WriteLine(arv);
            Console.WriteLine(teinearv);

            arv += 7; // arv = arv + 7;

            arv = 10;

            Console.WriteLine(++arv);  // ++ eespool, siis avaldises kehtib arvu uus väärtus
            Console.WriteLine(arv++); // ++ on tagapool, siis avaldises kehtib arvu vana väärtu


            int e1 = 3;   // 0011
            int e2 = 6;   // 0110

            Console.WriteLine(e1 & e2); // 0011 & 0110 -> 0010 ehk 2
            Console.WriteLine(e1 | e2); // 0011 | 0110 -> 0111 ehk 7

            byte bb = byte.MaxValue;
            Console.WriteLine(++bb);     // vaata proovi ja ananlüüsi, miks on nii
            sbyte sb = sbyte.MaxValue;
            Console.WriteLine(++sb);

            // plussid ees ja taga

            int arv1 = 7;
            int arv2 = 10;

            Console.WriteLine(arv1 + arv2); // 17
            Console.WriteLine(arv1++ + arv2++); // 17
            Console.WriteLine(++arv1 + ++arv2); // 21

            Console.WriteLine($"arv enne {arv1} tehtega {arv1++} ja siis {arv1}"); ;






        }
    }
}
