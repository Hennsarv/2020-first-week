﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MassiividestEdasi
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvudeMasssiiv = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> arvudeList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            int[] teine = new int[10];
            List<int> teineList = new List<int>();

        

            teineList.Add(17);

            arvudeList.Add(23);
            arvudeList.Remove(5);

            arvudeMasssiiv[3]++;
            arvudeList[7]++;

            //foreach (var x in arvudeMasssiiv) Console.WriteLine(x);
            Console.WriteLine(string.Join(", ", arvudeMasssiiv));
            Console.WriteLine(string.Join(", ", arvudeList));

            //foreach (var x in arvudeList) Console.WriteLine(x);

            Console.WriteLine("\nÜks näide, mis selgitab, mis listi sees toimub\n");

            List<int> uus = new List<int>(10);
            Console.WriteLine($"count: {uus.Count}, capacity: {uus.Capacity}");
            uus.Add(1);
            uus.Add(2);
            uus.Add(7);
            Console.WriteLine($"count: {uus.Count}, capacity: {uus.Capacity}");
            uus.Add(3);
            uus.Add(8);
            uus.Add(11);
            uus.Add(4);

            

            Console.WriteLine($"count: {uus.Count}, capacity: {uus.Capacity}");

            // sihukesel asjal nagu string on funktsioon Split
            string tekst = "Henn on  tore poiss";
            var sonad = tekst.Split(' ');
            foreach (var x in sonad) Console.WriteLine($">{x}<");

            var failistLoetudRead = File.ReadAllLines(@"..\..\spordipäeva protokoll.txt");
            foreach (var x in failistLoetudRead) Console.WriteLine(x);

            string[] nimed = new string[failistLoetudRead.Length];
            int[] ajad = new int[failistLoetudRead.Length];
            int[] distantsid = new int[failistLoetudRead.Length];
            double[] kiirused = new double[failistLoetudRead.Length];


            for (int i = 1 ; i < failistLoetudRead.Length; i++)
            {
                var reaOsad = failistLoetudRead[i].Split(',');
                string nimi = reaOsad[0].Trim(); nimed[i] = nimi;
                int distants = int.Parse(reaOsad[1].Trim()); distantsid[i] = distants;
                int aeg = int.Parse(reaOsad[2].Trim()); ajad[i] = aeg;
                double kiirus = distants * 1.0 / aeg; kiirused[i] = kiirus;
                Console.WriteLine($"{nimi} jooksis {distants} meetrit ajaga {aeg}");

            }

            double maxKiirus = kiirused.Max();
            Console.WriteLine("\nKõige kiirem(ad) jooksja(d):\n");

            for (int i = 1; i < kiirused.Length; i++)
                if (kiirused[i] == maxKiirus)
                    Console.WriteLine($"{nimed[i]} jooksis {distantsid[i]}m kiirusega {kiirused[i]:F2}");

            

        }


    }

    

}
