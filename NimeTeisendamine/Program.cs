﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NimeTeisendamine
{
    static class Program
    {
        static string ToProper(this string tekst)
        => tekst == "" ? "" : 
            string.Join(" ",
            tekst
            .Replace("-", "- ")
            .Split(' ')
            .Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower())
            ).Replace("- ", "-");
        static void Main(string[] args)
        {
            //Console.Write("Anna üks nimi: ");
            //for (                               // normaalne inimene ei tee sellist for tsüklit
            //                                    // see on Hennu veider usk, et alguses oli For 
            //                                    // ja kõik teised tema lapsed
            //        string nimi; 
            //        (nimi = Console.ReadLine()) != ""; 
            //        Console.Write("Anna uus nimi: ")
            //    )
            //{
            //    // siia oleks vaja teisendus (kohapeal - hiljem funktsiooniga)
            //    Console.WriteLine(nimi);
            //}

            // näide, kuidas normaalne inimene kirjutaks
            while(true)
            {
                Console.Write("anna üks nimi: ");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                // teisendus - nimega vaja teha midagi
                // 0. kõigepealt splitime
                var osad = nimi
                    .Replace("-", "- ")
                    .Split(' ');
                // iga osaga?
                for (int i = 0; i < osad.Length; i++)
                {
                    osad[i] = osad[i].Substring(0, 1).ToUpper() + osad[i].Substring(1).ToLower();
                }
                // 1. täht suureks ja ülejäänud väikeseks
                // 0. pärast kokku tagasi
                nimi = string.Join(" ", osad).Replace("- ", "-");
                Console.WriteLine(nimi);

                // tulevikus saab nii teha
                //Console.WriteLine(nimi.ToProper());
            }

        }
    }
}
