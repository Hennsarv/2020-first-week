﻿using System;
namespace Teisipäev
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
            int a = 7;
            a += 8;
            Console.WriteLine(a);

            int ohoo = 7;
            Int32 ahaa = 7;

            bool b1 = true;
            Boolean b2 = true;

            decimal d1 = 10M;
            Decimal d2 = 10M;

            Console.WriteLine(ohoo + ahaa);

            char c = 'a';
            c++;
            Console.WriteLine(c);

            // andmetüüp - võtmesõna (int) või klassinimi (System.Int32)
            // literaal - arvu tekstiline kujutis - 123
            // kuidas kujutada eri tüüpe arve
            // Decimal - 123M
            // Long - 123L
            // Double - 123D
            // Float - 123F
            // 123.0 - double

            // char literaal on märk ülakomade vahel 'Ä'

            // arvu literaal võib olla ka 16nd või 2nd arvuna antud
            int viisteist = 0xF;
            int i23 = 0b10111;
            Console.WriteLine(i23); // välja trükitakse 23
            Console.WriteLine(viisteist); // välja trükitakse 15

            // pikki arve on keeruline kirjutada, lubatud on vahemärk _
            long suur = 7_77_7_777_777; // L-pole vaja lõppu - niigi näha
            long keeruline = 0b_1011_1100_0101_1111;
            Console.WriteLine(keeruline);
            double d7 = 77.88;

            var imelikarv = 77D;

            // avaldised

            int a0 = 7;
            int a1 = 8;
            Console.WriteLine(a0 + a1);

            
        }

  
    }

}