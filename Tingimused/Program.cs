﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tingimused
{
    class Program
    {
        static void Main(string[] args)
        {
            // tingimuslause või blokk
            //Console.Write("Palju sa plaka saad: ");
            //int palk = int.Parse(Console.ReadLine());
            //if (palk > 1000) 
            //{
            //    Console.WriteLine("sinuga võiks kohe õhtut veetma minna");
            //}
            //else if (palk > 500)
            //{
            //    Console.WriteLine("no lähme siis kinno");
            //}
            //else
            //{
            //    Console.WriteLine("sinuga tasub vaid parki jalutama minna");
            //}

            //// valgusfoor-1
            //string värv = "";
            ////while (värv != "roheline")
            //{
            //    Console.Write("Mis värvi tuli seal on: ");
            //    värv = Console.ReadLine().ToLower();
            //    if (värv == "roheline") { Console.WriteLine("sõida edasi"); }
            //    else if (värv == "kollane") { Console.WriteLine("oota rohelist"); }
            //    else if (värv == "punane") { Console.WriteLine("jää seisma"); }
            //    else Console.WriteLine("pese silmad puhtaks");
            //}

            //string test = "saksa pikk ß kreeka β";
            //Console.WriteLine(test);
            //test = test.ToUpper();
            //Console.WriteLine(test);

            // teine variant tingimuslausest (kolmas tuleb ka kohe)
            string värv = "";
            while (värv != "roheline")
            {
                switch (värv = Console.ReadLine().ToLower())
                {
                    case "punane":
                    case "red":
                        Console.WriteLine("jää seisma");
                        goto case "kollane";
                    case "roheline":
                    case "green":
                        Console.WriteLine("sõida edasi");
                        break;
                    case "kollane":
                    case "yellow":
                        Console.WriteLine("oota rohelist");
                        break;
                    default:
                        Console.WriteLine("pese silmad puhtaks");
                        break;
                }
            }
            // kolmas variant

            Console.Write("Ütle uuesti see värv seal: ");
            värv = Console.ReadLine().ToLower();
            Console.WriteLine(
                (
                värv == "punane" ? "jää seisma" :
                värv == "kollane" ? "oota rohelist" :
                värv == "roheline" ? "sõida edasi" :
                "pese silmad puhtaks"
                )
                );

            // Mis päev täna on
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    Console.WriteLine("Joodagu õlutit");
                    break;
                case DayOfWeek.Wednesday:
                    Console.WriteLine("mindagi trenni");
                    goto default;
                case DayOfWeek.Saturday:
                    Console.WriteLine("Teeme sauna");
                    goto case DayOfWeek.Sunday;
                default:
                    Console.WriteLine("minnakse tööle");
                    break;
            }

        }
    }
}
