﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiSegamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            var enne = new List<int>();
            
            while (enne.Count < 52)             // teeme niikaua kuini on 52 kaarti
            {
                int k = r.Next(52);             // tõmmame ühe kaardi
                                                // kui meil seda ei ole, siis lisame ta oma pakki
                if (!enne.Contains(k)) enne.Add(k);
            }

            // on kellelgi mõni idee?

            enne = Enumerable.Range(0, 52).ToList();
            var uus = new List<int>();
            while(enne.Count > 0)               // teeme niikaua, kuni pakis on veel kaarte
            {
                int x = r.Next(enne.Count);     // valime juhusliku kaardi
                uus.Add(enne[x]);               // tõstame ta teise pakki
                enne.RemoveAt(x);               // kustutame vanast pakist
            }
            enne = uus;

            enne = Enumerable.Range(0, 52).ToList();
            for (int i = 0; i < 10000; i++)
            {
                int x = r.Next(52);
                int y = r.Next(52);
                int t = enne[x];
                enne[x] = enne[y];
                enne[y] = t;
            }

            // siin vahepeal tuleks midagi teha, et need arvud VÕIMALIKULT segi oleks
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{enne[i]:00}" + (i % 4 == 3 ? "\n" : "\t" ));
            }
        }
    }
}
